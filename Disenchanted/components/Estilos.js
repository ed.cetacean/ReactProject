
// -------------------------------------------------------------------------- //

import { negroOpaco, blancoOpaco, azulOpaco } from './Colores';
import { StyleSheet } from 'react-native';


// -------------------------------------------------------------------------- //

export const Estilos = StyleSheet.create({

    // NAVEGACIÓN: ---------------------------------------------------------- //

    headerContainer: {
        width: '80%',
        marginTop: 20,
        borderRadius: 30,
        alignSelf: 'center',
        paddingVertical: 15,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 30,
        justifyContent: 'space-between',

        shadowColor: negroOpaco,
        shadowOpacity: 0.4,
        shadowRadius: 12,
        elevation: 22,
    },

    headerTitle: {
        fontSize: 18,
        fontFamily: 'Bold',
    },

    safeContainer: {
        flex: 1,
    },

    profileContainer: {
        paddingTop: 80,
        paddingBottom: 40,
    },

    profilePicture: {
        width: 200,
        height: 200,
        borderWidth: 4,
        marginBottom: 40,
        borderRadius: 200,
        alignSelf: 'center',
    },
    
    profileName: {
        fontSize: 22,
        fontFamily: 'Bold',
        textAlign: 'center',
        color: blancoOpaco,
    },

    logoutContainer: {
        padding: 10,
    },

    drawerLabel: {
        fontSize: 15,
        fontFamily: 'SemiBold',
    },

    // THEME-BUTTON: -------------------------------------------------------- //

    themeButton: {
        width: 24, height: 24,
    },

    // INICIAR-SESIÓN ------------------------------------------------------- //

    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    iconCW: {
        marginBottom: 40,
        width: 240, height: 240,
    },

    inputView: {
        width: 240,
        paddingBottom: 8,
        marginBottom: 24,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: blancoOpaco,
    },

    inputIcon: {
        width: 28,
    },

    textInput: {
        flex: 1,
        paddingLeft: 6,
        color: blancoOpaco,
        fontFamily: 'Regular',
    },

    loginButton: {
        width: 240,
        height: 42,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: azulOpaco,
    },

    loginText: {
        fontFamily: 'Bold',
        color: blancoOpaco,
    },

    passwordButton: {
        fontSize: 12,
        paddingTop: 24,
        color: blancoOpaco,
        fontFamily: 'SemiBold',
    },

    ButtonDisabled: {
        width: 240,
        height: 42,
        borderRadius: 6,
        alignItems: 'center',
        backgroundColor: 'grey',
        justifyContent: 'center',
    },

    // FORGOT-PASSWORD ------------------------------------------------------ //

    userView: {
        width: 260,
        flexDirection: 'row',
        alignItems: 'center',
    },

    profileIMG: {
        width: 130,
        height: 130,
        borderWidth: 4,
        marginRight: 20,
        marginBottom: 40,
        borderRadius: 130,
        alignSelf: 'center',
    },

    cancelButton: {
        width: 125,
        height: 42,
        borderWidth: 2,
        marginRight: 10,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: azulOpaco,
    },

    confirmButton: {
        width: 125,
        height: 42,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: azulOpaco,
    },

    // CERRAR-SESIÓN -------------------------------------------------------- //

    outView: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    outButton: {
        padding: 12,
        width: '100%',
        marginVertical: 6,
        flexDirection: 'row',
        alignItems: 'center',
    },

    outIcon: {
        marginRight: 12,
    },

    outText: {
        fontSize: 16,
        fontFamily: 'SemiBold',
    },

    // MI-PERFIL ------------------------------------------------------------ //

    darkOverlay: {
        flex: 1,
        backgroundColor: 'rgba(28, 28, 30, 0.6)',
    },

    profileHeader: {
        width: 390,
        marginTop: 140,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'flex-end',
    },

    userImage: {
        width: 140,
        height: 140,
        borderWidth: 4,
        borderRadius: 140,
        borderColor: blancoOpaco,
    },

    profileInfo: {
        flex: 1,
        marginLeft: 20,
    },

    userName: {
        fontSize: 22,
        fontFamily: 'Bold',
        textAlign: 'center',
        color: blancoOpaco,
    },

    levelContainer: {
        paddingHorizontal: 12,
        paddingVertical: 6,
        marginTop: 14,
        borderRadius: 30,
    },

    level: {
        fontSize: 16,
        textAlign: 'center',
        fontFamily: 'SemiBold',
    },

    profileSection: {
        width: 390,
        marginTop: 40,
        marginBottom: 20,
        alignSelf: 'center',

    },

    aboutMe: {
        paddingVertical: 8,
        alignItems: 'center',
        flexDirection: 'row',
    },

    icon: {
        marginRight: 10,
    },

    titleAbout: {
        fontSize: 18,
        color: blancoOpaco,
        fontFamily: 'SemiBold',
        textAlignVertical: 'center',
    },

    aboutBio: {
        marginTop: 8,
        fontSize: 16,
        color: blancoOpaco,
        textAlign: 'justify',
        fontFamily: 'Italic',
    },

    aboutContent: {
        fontSize: 16,
        marginTop: 8,
        color: blancoOpaco,
        textAlign: 'justify',
        fontFamily: 'Regular',
    },

    buttonDetail: {
        width: 390,
        marginTop: 12,
        borderRadius: 30,
        paddingVertical: 14,
        alignSelf: 'center',
    },

    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        fontFamily: 'SemiBold',
    }

    // -------------------------------------------------------------------------- //



    // -------------------------------------------------------------------------- //

});

// -------------------------------------------------------------------------- //