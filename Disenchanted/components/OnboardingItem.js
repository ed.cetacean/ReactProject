//OnboardingItem.js

import React from 'react';
import Colores from './Colores';
import { View, Text, StyleSheet, Image } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faUserEdit, faLockOpen, faWallet, faHeadset } from '@fortawesome/free-solid-svg-icons';

// -------------------------------------------------------------------------- //

const OnboardingItem = ({ item }) => {
    const themeColor = Colores();

    const getFontAwesomeIcon = (iconName) => {
        switch (iconName) {
            case 'user-edit':
                return faUserEdit;
            case 'lock-open':
                return faLockOpen;
            case 'wallet':
                return faWallet;
            case 'headset':
                return faHeadset;
            default:
                return null;
        }
    };

    return (
        <View style={[ styles.container, { backgroundColor: themeColor.textoDinamico } ]}>
            <View style={styles.subContainer}>

                <Text style={[ styles.title, { color: themeColor.fondoDinamico } ]}>{item.title}</Text>
                <Text style={[ styles.description, { color: themeColor.fondoDinamico } ]}>{item.description}</Text>

                {item.actions && item.actions.map((action, index) => (

                    <View key={index} style={styles.actionContainer}>
                        <View style={styles.actionContent}>
                            <FontAwesomeIcon icon={getFontAwesomeIcon(action.icon)} size={18} color={themeColor.fondoDinamico } />
                            <Text style={[ styles.actionTitle, { color: themeColor.fondoDinamico } ]}>{action.title}</Text>
                        </View>

                        <View style={[ styles.actionBox, { backgroundColor: themeColor.fondoDinamico } ]}>
                            <Text style={[ styles.actionDescription, { color: themeColor.textoDinamico } ]}>{action.description}</Text>
                        </View>
                    </View>

                ))}
                
                {item.image && (
                    <Image source={item.image} style={styles.image} />
                )}

            </View>
        </View>
    );
};

const styles = StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },

    subContainer: {
        width: '60%',
        marginTop: 140,
        alignItems: 'center',
        justifyContent: 'center',
    },

    title: {
        fontSize: 24,
        marginBottom: 12,
        fontFamily: 'Bold',
        textAlign: 'center',
    },

    description: {
        fontSize: 18,
        lineHeight: 22,
        marginBottom: 22,
        flexWrap: 'wrap',
        textAlign: 'center',
        fontFamily: 'Regular',
    },

    image: {
        width: 320,
        height: 320,
        marginTop: 40,
        borderRadius: 32,
    },

    actionContainer: {
        marginTop: 12,
        marginBottom: 22,
        alignSelf: 'center',
    },

    actionContent: {
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    actionBox: {
        borderRadius: 30,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 24,
    },

    actionTitle: {
        fontSize: 16,
        marginLeft: 12,
        fontFamily: 'SemiBold',
    },

    actionDescription: {
        fontSize: 14,
        marginLeft: 12,
        overflow: 'hidden',
        fontFamily: 'Regular',
    },

});

export default OnboardingItem;