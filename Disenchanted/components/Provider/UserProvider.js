
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

// -------------------------------------------------------------------------- //

const UserContext = createContext(null);

export const UserProvider = ({ children }) => {
    const [ user, setUser ] = useState(null);

    // ---------------------------------------------------------------------- //

    const updateUser = async (userData) => {

        try {
            await AsyncStorage.setItem('userData', JSON.stringify(userData));
            setUser(userData);
        } catch (error) {
            console.error("Ha ocurrido un error al actualizar el usuario: ", error);
        }

    };

    const cerrarSesion = async () => {

        try {
            await AsyncStorage.removeItem('userData');
            setUser(null);
        } catch (error) {
            console.error("Ha ocurrido un error al intentar cerrar la sesión actual: ", error);
        }

    };

    // ---------------------------------------------------------------------- //

    useEffect(() => {
        const loadUser = async () => {

            try {
                const userDataString = await AsyncStorage.getItem('userData');
                if (userDataString) {
                    const userData = JSON.parse(userDataString);
                    setUser(userData);
                }
            } catch (error) {
                console.error("Ha ocurrido un error al cargar los datos del usuario: ", error);
            }
        };

        loadUser();
    }, []);

    // ---------------------------------------------------------------------- //

    return (
        <UserContext.Provider value={{ user, updateUser, cerrarSesion }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => useContext(UserContext);

// -------------------------------------------------------------------------- //
