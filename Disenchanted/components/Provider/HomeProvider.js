
import React, { useEffect } from 'react';
import { useUser } from './UserProvider';
import { useNavigation } from '@react-navigation/native';

// -------------------------------------------------------------------------- //

const HomeProvider = () => {
    const { user } = useUser();
    const navigation = useNavigation();

    useEffect(() => {

        if (user) {
            navigation.navigate('Home');
        } else {
            navigation.navigate('Login');
        }

    }, [user, navigation]);

    return null;

};

export default HomeProvider;

// -------------------------------------------------------------------------- //