
import React from 'react';
import Colores from '../Colores';
import { Estilos } from '../Estilos';
import { useUser } from './UserProvider';
import { FontAwesome } from '@expo/vector-icons';
import { View, Pressable, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

// -------------------------------------------------------------------------- //

const LogoutButton = () => {
    const themeColor = Colores();
    const { cerrarSesion } = useUser();
    const navigation = useNavigation();

    const handleLogout = () => {
        cerrarSesion();
        navigation.navigate('Login');
    };

    return (
        <View style={Estilos.outView}>
            <Pressable style={Estilos.outButton} onPress={handleLogout}>
                <FontAwesome name="sign-out" size={16} color={ themeColor.itemDinamico } style={Estilos.outIcon} />
                <Text style={[ Estilos.outText, { color: themeColor.itemDinamico } ]}>Salir</Text>
            </Pressable>
        </View>
    );
};

export default LogoutButton;

// -------------------------------------------------------------------------- //