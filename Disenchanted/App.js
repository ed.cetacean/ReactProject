
// -------------------------------------------------------------------------- //

import React from 'react';
import { useFonts } from 'expo-font';
import Navegacion from './components/Navegacion';
import { ActivityIndicator } from 'react-native';
import { blancoOpaco, negroOpaco } from './components/Colores';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { UserProvider } from './components/Provider/UserProvider';
import { ThemeProvider } from './components/Provider/ThemeContext';
import Toast, { BaseToast, ErrorToast } from 'react-native-toast-message';

// -------------------------------------------------------------------------- //

// Alertas:
const toastConfig = {

    success: (props) => (
        <BaseToast {...props}
            style={{
                backgroundColor: negroOpaco,
                borderLeftColor: 'green',
                borderLeftWidth: 8,
                marginTop: 20,
                width: '80%',
                height: 80,
            }}

            contentContainerStyle={{
                paddingHorizontal: 16,
            }}

            text1Style={{
                fontSize: 16,
                fontWeight: '600',
                color: blancoOpaco,
            }}

            text2Style={{
                fontSize: 14,
                color: blancoOpaco,
            }}
        />
    ),

    error: (props) => (
        <ErrorToast {...props}
            text2NumberOfLines={3}

            style={{
                backgroundColor: negroOpaco,
                borderLeftColor: 'red',
                borderLeftWidth: 8,
                marginTop: 20,
                width: '80%',
                height: 80,
            }}

            contentContainerStyle={{
                paddingHorizontal: 16,
            }}

            text1Style={{
                fontSize: 16,
                fontWeight: '600',
                color: blancoOpaco,
            }}

            text2Style={{
                fontSize: 14,
                color: blancoOpaco,
            }}
        />
    ),

};

// -------------------------------------------------------------------------- //

export default function App() {

    // Fuentes:
    const [ fontsLoaded ] = useFonts({
        'ExtraBold': require('./assets/Font/Reddit Sans/RedditSans-ExtraBold.ttf'),
        'ExtraBoldItalic': require('./assets/Font/Reddit Sans/RedditSans-ExtraBoldItalic.ttf'),
        
        'Bold': require('./assets/Font/Reddit Sans/RedditSans-Bold.ttf'),
        'BoldItalic': require('./assets/Font/Reddit Sans/RedditSans-BoldItalic.ttf'),

        'SemiBold': require('./assets/Font/Reddit Sans/RedditSans-SemiBold.ttf'),
        'SemiBoldItalic': require('./assets/Font/Reddit Sans/RedditSans-SemiBoldItalic.ttf'),
        
        'Regular': require('./assets/Font/Reddit Sans/RedditSans-Regular.ttf'),
        'Italic': require('./assets/Font/Reddit Sans/RedditSans-Italic.ttf'),

        'Light': require('./assets/Font/Reddit Sans/RedditSans-Light.ttf'),
        'LightItalic': require('./assets/Font/Reddit Sans/RedditSans-LightItalic.ttf'),
    });

    if (!fontsLoaded) {
        return <ActivityIndicator size="large" />;
    }

    // ---------------------------------------------------------------------- //

    return (
        <UserProvider>
            <ThemeProvider>
                <SafeAreaProvider>
                    <Navegacion />
                    <Toast config={toastConfig} />
                </SafeAreaProvider>
            </ThemeProvider>
        </UserProvider>
    );

}

// -------------------------------------------------------------------------- //