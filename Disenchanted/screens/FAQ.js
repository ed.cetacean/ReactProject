
import React, { useState } from 'react';
import Colores from '../components/Colores';
import Animated from 'react-native-reanimated'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';

const QuestionsAndAnswers = () => {
    const themeColor = Colores();
    const [ expanded, setExpanded ] = useState(new Array(3).fill(false));

    const toggleQuestion = (index) => {
        const newExpanded = [...expanded];

        newExpanded[index] = !newExpanded[index];
        setExpanded(newExpanded);
    };

    return (
        <View style={[ styles.container, { backgroundColor: themeColor.textoDinamico } ]}>

            <ScrollView style={styles.scrollView}>
                <View> {questions.map((question, index) => (
                    
                    <TouchableOpacity key={index} style={styles.questionContainer} onPress={() => toggleQuestion(index)} >
                        <View style={styles.questionTextContainer}>
                            <Text style={[ styles.questionText, { color: themeColor.fondoDinamico } ]}>{question.questionText}</Text>
                        </View>

                        {expanded[index] && (
                            <Animated.View style={styles.answerContainer} >
                                <Text style={[ styles.answerText, { color: themeColor.fondoDinamico } ]}>{question.answerText}</Text>
                            </Animated.View>
                        )}

                    </TouchableOpacity> ))}
                </View>
            </ScrollView>

        </View>
    );
};

const questions = [
    {
        questionText: '¿Cómo puedo rentar un casillero?',
        answerText: ' + Puedes llevar a cabo la renta de tu casillero en la sección de Casillero, siempre & cuando aún quede alguno disponible',
    },

    {
        questionText: '¿Puedo rentar más de un casillero?',
        answerText: ' + No, no es posible rentar más de un casillero. Actualmente la institución permite únicamente la renta de un casillero por persona.',
    },

    {
        questionText: '¿Cuál es el costo de rentar un casillero?',
        answerText: ' + El costo por rentar un casillero puede variar dependiendo del tamaño de este mismo, así como de ciertos cambios que pueda llevar a cabo la institución el precio de estos mismo.',
    },

    {
        questionText: '¿Cómo puedo cancelar la renta de un casillero?',
        answerText: ' + Actualmente no es posible cancelar la renta de un casillero por medio de la aplicación, por ello te pedimos que observes atentamente la información del casillero antes de finalizar el pago de la renta de este mismo.',
    },

    {
        questionText: '¿Puedo cambiar mi contraseña de acceso?',
        answerText: ' + Así es, puedes cambiar tu contraseña en la sección de Inicio de Sesión de la aplicación, confirmando el código de verificación que sera envíado a tu correo institucional.',
    },

];

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    scrollView: {
        width: '80%',
        marginTop: 140,
        alignSelf: 'center',
    },

    questionContainer: {
        marginBottom: 30,
    },

    questionTextContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    questionText: {
        fontSize: 18,
        lineHeight: 30,
        fontFamily: 'SemiBold',
    },

    answerContainer: {
        marginTop: 10,
        height: 'auto',
        marginHorizontal: 10,
    },

    answerText: {
        fontSize: 14,
        fontFamily: 'Regular',
    },

});

export default QuestionsAndAnswers;
