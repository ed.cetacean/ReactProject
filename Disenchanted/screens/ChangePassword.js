
import { BASE_URL } from '../Config';
import React, { useState } from 'react';
import Toast from 'react-native-toast-message';
import { Estilos } from '../components/Estilos';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { negroOpaco, blancoOpaco, azulOpaco } from '../components/Colores';
import { MaterialIcons, Entypo, FontAwesome5, Ionicons } from '@expo/vector-icons';
import { View, TextInput, Image, Pressable, Text, Platform, Alert, ActivityIndicator } from 'react-native';

const CodeWave = require('../assets/CodeWave.png');

// -------------------------------------------------------------------------- //

const ChangePassword = ({ route }) => {
    const [ codigo, setCodigo ] = useState('');
    const [ nuevaPassword, setNuevaPassword ] = useState('');
    const isCodigoDisabled = codigo.trim() === '';
    const isPasswordDisabled = nuevaPassword.trim() === '';
    const [ passwordVisible, setPasswordVisible ] = useState(false);
    const [ isLoading, setIsLoading]  = useState(false);
    const [ verificado, setVerificado ] = useState(false);
    const navigation = useNavigation();
    const { email } = route.params;


    const resetConst = () => {
        setCodigo('');
        setNuevaPassword('');
        setVerificado(false);
        setPasswordVisible(false);
    }

    const togglePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const handleCodigoChange = (text) => {
        const filteredText = text.replace(/[^0-9]/g, '');
        setCodigo(filteredText);
    };

    const verificarCodigo = async () => {
        setIsLoading(true);

        try {
            const response = await fetch(`${BASE_URL}/v1/verificarCodigo/`, {
                method: 'POST', headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email, codigo }),
            });

            const data = await response.json();
            setIsLoading(false);

            if(response.ok) {
                setVerificado(true);
                Toast.show({
                    type: 'success',
                    text1: 'ÉXITO',
                    text2: data.message,
                    visibilityTime: 4500,
                })
            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: data.error,
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })
            setIsLoading(false);
        }
    };

    const cambiarPassword = async () => {
        setIsLoading(true);

        try {
            const response = await fetch(`${BASE_URL}/v1/cambiarPassword/`, {
                method: 'POST', headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email, nuevaPassword }),
            });

            const data = await response.json();
            setIsLoading(false);

            if(response.ok) {
                Toast.show({
                    type: 'success',
                    text1: 'ÉXITO',
                    text2: data.message,
                    visibilityTime: 4500,
                })
                navigation.navigate('Login');
                resetConst();
            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: data.error,
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })
            setIsLoading(false);
        }
    };

    // ---------------------------------------------------------------------- //

    return (

        <SafeAreaView style={[ Estilos.safeContainer, { backgroundColor: negroOpaco } ]}>

            <View style={[ Estilos.headerContainer, { backgroundColor: azulOpaco } ]}>
                <Text style={[ Estilos.headerTitle, { color: blancoOpaco } ]}>Reestablecer contraseña</Text>

                <Pressable onPress={() => { resetConst(); navigation.navigate('Login'); }}>
                    <Ionicons name="close" size={24} color={ blancoOpaco } />
                </Pressable>
            </View>

            {/* ------------------------------------------------------------ */}

            <View style={ Estilos.mainContainer }>
                <Image source={CodeWave} style={Estilos.iconCW} />

                {!verificado ? ( <>

                    <View style={Estilos.inputView}>
                        <View style={Estilos.inputIcon}>
                            <MaterialIcons name="security" size={18} color={blancoOpaco} />
                        </View>

                        <TextInput placeholder='Código de verificación' inputMode='numeric' maxLength={4}
                            placeholderTextColor={blancoOpaco} onChangeText={handleCodigoChange} value={codigo} 
                            style={[Estilos.textInput, Platform.OS === 'web' && {outlineStyle: 'none'}]} />
                    </View>

                    <Pressable style={isCodigoDisabled ? Estilos.ButtonDisabled : Estilos.loginButton}
                    onPress={verificarCodigo} disabled={isCodigoDisabled || isLoading}>
                        {isLoading ? (
                            <ActivityIndicator size={18} color={blancoOpaco} />
                        ) : (
                            <Text style={Estilos.loginText}>VERIFICAR CÓDIGO</Text>
                        )}
                    </Pressable>

                </> ) : ( <>

                    <View style={Estilos.inputView}>
                        <View style={Estilos.inputIcon}>
                            <Entypo name='lock' size={18} color={blancoOpaco} />
                        </View>

                        <TextInput placeholder='Nueva contraseña' secureTextEntry={!passwordVisible} maxLength={32}
                            placeholderTextColor={blancoOpaco} onChangeText={setNuevaPassword} value={nuevaPassword}
                            style={[Estilos.textInput, Platform.OS === 'web' && {outlineStyle: 'none', width: 182 } ]} />

                        <Pressable onPress={togglePasswordVisibility} style={{ marginLeft: 10 }}>
                            { passwordVisible ? <FontAwesome5 name='eye-slash' size={14} color={blancoOpaco} /> : <FontAwesome5 name='eye' size={16} color={blancoOpaco} /> }
                        </Pressable>
                    </View>

                    <Pressable style={isPasswordDisabled ? Estilos.ButtonDisabled : Estilos.loginButton}
                    onPress={cambiarPassword} disabled={isPasswordDisabled || isLoading}>
                        {isLoading ? (
                            <ActivityIndicator size={18} color={blancoOpaco} />
                        ) : (
                            <Text style={Estilos.loginText}>CAMBIAR CONTRASEÑA</Text>
                        )}
                    </Pressable>
                </> )}

            </View>

        </SafeAreaView>
    );

};

export default ChangePassword;

// -------------------------------------------------------------------------- //