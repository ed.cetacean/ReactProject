
import React from 'react';
import Colores from '../components/Colores';
import { Entypo, FontAwesome6 } from '@expo/vector-icons';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

import LogoCW from '../assets/CodeWave.png';


const About = () => {
    const themeColor =  Colores();

    return (
        <View style={[ styles.container, { backgroundColor: themeColor.textoDinamico } ]}>
            <ScrollView style={styles.scrollContainer}>

                <Image style={styles.image} source={LogoCW} />

                <Text style={[ styles.text, { color: themeColor.fondoDinamico } ]}>
                    {`¡Hola! Somos CodeWave, una empresa de desarrollo de aplicaciones móviles.`}
                </Text>

                <Text style={[ styles.text, { color: themeColor.fondoDinamico } ]}>
                    {`
                        Nuestro equipo está formado por desarrolladores experimentados que han trabajado en una amplia variedad de proyectos, desde pequeñas startups hasta grandes empresas. Nos enorgullecemos de ofrecer la mejor experiencia de usuario posible mediante el uso de las tecnologías y prácticas más recientes. Si tienes alguna pregunta o deseas obtener más información sobre nuestros servicios, no dudes en ponerte en contacto con nosotros.
                    `}
                
                </Text>

                <Text style={[ styles.text, { color: themeColor.fondoDinamico } ]}>
                    {`¡Estamos deseando escuchar de ti!`}
                </Text>


                <View style={styles.socialIcons}>
                    <TouchableOpacity style={styles.iconButton}>
                        <Entypo name="facebook-with-circle" size={30} color="#3b5998" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.iconButton}>
                        <Entypo name="twitter-with-circle" size={30} color="#1da1f2" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.iconButton}>
                        <Entypo name="instagram-with-circle" size={30} color="#e4405f" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.iconButton}>
                        <FontAwesome6 name="gitlab" size={30} color="#F99726" />
                    </TouchableOpacity>
                </View>

                <Text style={[ styles.crText, { color: themeColor.fondoDinamico } ]}>© CodeWave, 2024</Text>

            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    scrollContainer: {
        width: '70%',
        marginTop: 140,
        alignSelf: 'center',
    },

    image: {
        width: 140,
        height: 140,
        alignSelf: 'center',
    },

    text: {
        fontSize: 16,
        marginTop: 20,
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Regular',
    },

    socialIcons: {
        width: '40%',
        marginTop: 80,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        
    },

    iconButton: {
        backgroundColor: 'transparent',
    },

    crText: {
        bottom: 0,
        fontSize: 12,
        marginTop: 40,
        textAlign: 'center',
        fontFamily: 'SemiBoldItalic',
    },
});

export default About;