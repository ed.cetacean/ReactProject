
import { BASE_URL } from "../Config";
import Colores from "../components/Colores";
import Toast from "react-native-toast-message";
import { Picker } from '@react-native-picker/picker';
import { useNavigation } from '@react-navigation/native';
import React, { useState, useEffect, useRef } from "react";
import { useUser } from "../components/Provider/UserProvider";
import { Text, StyleSheet, Image, TouchableOpacity, View, ActivityIndicator } from "react-native";
import { PayPalScriptProvider, PayPalButtons, usePayPalScriptReducer } from "@paypal/react-paypal-js";

const LockerImage = require('../assets/Locker.png');

// -------------------------------------------------------------------------- //

const AvailLockers = () => {
    const { user } = useUser();
    const themeColor = Colores();
    const navigation = useNavigation();
    const [ casilleros, setCasilleros ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(false);
    const [ casilleroActual, setCasilleroActual ] = useState(null);
    const [ casillerosFiltrados, setCasillerosFiltrados ] = useState([]);
    const [ filtros, setFiltros ] = useState({ tamano: "", docencia: "", piso: "", idCasillero: "" });

    // ---------------------------------------------------------------------- //

    useEffect(() => {
        consultarCasilleros();
    }, []);

    const consultarCasilleros = async () => {
        setIsLoading(true);

        try { const response = await fetch(`${BASE_URL}/v1/casilleros/list/`, {
                method: 'GET', headers: { 'Content-Type': 'application/json' },
            });

            const data = await response.json();
            setIsLoading(false);

            if (response.ok) {
                setCasilleros(data);
                setCasillerosFiltrados(data);

            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: 'Ha ocurrido un error en la consulta de casilleros disponibles.',
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })

            setIsLoading(false);
        }
    };

    useEffect(() => {
        const filtered = casilleros.filter(casillero => {
            return (!filtros.tamano || casillero.Tamano === filtros.tamano) &&
                (!filtros.docencia || casillero.Docencia === filtros.docencia) &&
                (!filtros.piso || casillero.Piso === filtros.piso);
        });

        setCasillerosFiltrados(filtered);
    }, [ filtros ]);

    // ---------------------------------------------------------------------- //

    const usarFiltro = (type, value) => {
        setFiltros(prevFilters => ({ ...prevFilters,
            [type]: prevFilters[type] === value ? null : value
        }));
    };

    const idFiltradas = () => {
        return casillerosFiltrados.map(casillero => casillero.id.toString());
    };

    useEffect(() => {
        const casilleroActualID = filtros.idCasillero;
        const casillero = casillerosFiltrados.find(casillero => casillero.id.toString() === casilleroActualID);
        setCasilleroActual(casillero);
    }, [ filtros.idCasillero, casillerosFiltrados ]);

    // ---------------------------------------------------------------------- //

    const casilleroReferencia = useRef(casilleroActual);
    useEffect(() => {
        casilleroReferencia.current = casilleroActual;
    }, [casilleroActual]);

    const createOrder = (data, actions) => {
        const Datos = casilleroReferencia.current;

        if (!casilleroActual) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'Seleccione un casillero antes de proceder al pago.',
                visibilityTime: 4500,
            });
        }

        return actions.order.create({
            purchase_units: [{
                amount: { value: Datos?.Precio.toString() },
            }],
        });
    };

    const onApprove = (data, actions) => {
        const Datos = casilleroReferencia.current;

        return actions.order.capture().then(details => {

            fetch(`${BASE_URL}/v1/casillero/paypal/`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },

                body: JSON.stringify({
                    usuarioID: user.id,
                    casilleroID: Datos?.id,
                })
            })

            .then(response => response.json())
            .then(data => {
                Toast.show({
                    type: 'success',
                    text1: 'ÉXITO',
                    text2: `Pago aprobado para el casillero No. ${Datos?.id}`,
                    visibilityTime: 9000,
                });

                consultarCasilleros();
                navigation.navigate('EditLocker');
            })

            .catch(error => {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: 'Ha ocurrido un error durante el proceso de pago.',
                    visibilityTime: 4500,
                });
            });

        }).catch(error => {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo completar el pago en PayPal.',
                visibilityTime: 4500,
            });
        });
    };
    

    if (isLoading) {
        return <View style={styles.centeredContainer}><ActivityIndicator size="large" /></View>;
    };

    // ---------------------------------------------------------------------- //

    return (

        <PayPalScriptProvider options={{ "client-id": "Afj4QiCVTaiW9HYDcgd4x5osEMQdvr96r4EzUwQsM5UxbPm3KG50eiXNv5MKiiAwQ-N0IMtyLiE-fooG", currency: "MXN" }}>

            <View style={{ flex: 1, backgroundColor: themeColor.textoDinamico }}>

                <View style={styles.amazonContainer}>
                    <Image source={LockerImage} style={styles.lockerImage} />


                    <Text style={[ styles.filterTitle, { color: themeColor.fondoDinamico } ]}>DOCENCIA</Text>

                    <View style={styles.filterSection}>
                        {['Docencia 1', 'Docencia 2', 'Docencia 3', 'Docencia 4', 'Docencia 5'].map((docencia, index) => (
                            <TouchableOpacity key={index} onPress={() => usarFiltro('docencia', docencia)}
                                style={[
                                    styles.filterButton, { borderColor: themeColor.fondoDinamico },
                                    filtros.docencia === docencia && { backgroundColor: themeColor.fondoDinamico }
                                ]}>
                                <Text style={[
                                    styles.filterText, { color: themeColor.fondoDinamico },
                                    filtros.docencia === docencia && { color: themeColor.textoDinamico }
                                ]}>{docencia.split(' ')[1]}</Text>
                            </TouchableOpacity>
                        ))}

                    </View>


                    <View style={styles.doubleFilter}>

                        <View style={{ marginRight: 18 }}>
                            <Text style={[ styles.filterTitle, { color: themeColor.fondoDinamico } ]}>PLANTA</Text>

                            <View style={styles.filterSection}>
                                {['Planta Baja', 'Planta Alta'].map((planta) => (
                                    <TouchableOpacity key={planta} onPress={() => usarFiltro('piso', planta)} 
                                        style={[
                                            styles.filterButton, { borderColor: themeColor.fondoDinamico },
                                            filtros.piso === planta && { backgroundColor: themeColor.fondoDinamico }
                                        ]}>

                                        <Text style={[
                                            styles.filterText, { color: themeColor.fondoDinamico },
                                            filtros.piso === planta && { color: themeColor.textoDinamico }
                                            ]}>{planta.split(' ')[1]}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>

                        <View>
                            <Text style={[ styles.filterTitle, { color: themeColor.fondoDinamico } ]}>TAMAÑO</Text>

                            <View style={styles.filterSection}>
                                {['Normal', 'Grande'].map((tamano) => (
                                    <TouchableOpacity key={tamano} onPress={() => usarFiltro('tamano', tamano)} 
                                        style={[
                                            styles.filterButton, { borderColor: themeColor.fondoDinamico },
                                            filtros.tamano === tamano && { backgroundColor: themeColor.fondoDinamico }
                                            ]}>
                                        <Text style={[
                                            styles.filterText, { color: themeColor.fondoDinamico },
                                            filtros.tamano === tamano && { color: themeColor.textoDinamico }
                                        ]}>{tamano}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                    </View>

                    <View style={[ styles.pickerView, { borderColor: themeColor.fondoDinamico, marginBottom: 30 } ]}>
                        <Picker selectedValue={filtros.idCasillero}
                            onValueChange={(itemValue) => setFiltros({...filtros, idCasillero: itemValue})}
                            style={[ styles.picker, { color: themeColor.fondoDinamico } ]} >
                            
                            <Picker.Item label="Seleccione un casillero" />
                            {idFiltradas().map(id => ( <Picker.Item key={id} label={id} value={id} /> ))}
                        </Picker>
                    </View>


                    <PayPalButtons
                        style={{ layout: 'vertical' }}
                        createOrder={createOrder}
                        onApprove={onApprove}
                    />


                </View>

            </View>

        </PayPalScriptProvider>
    );
};

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    amazonContainer: {
        flex: 1,
        width: '80%',
        marginTop: 140,
        alignSelf: 'center',
        alignItems: 'center',
    },

    filterSection: {
        marginTop: 12,
        alignSelf: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },

    filterButton: {
        borderWidth: 2,
        borderRadius: 20,
        paddingVertical: 10,
        marginHorizontal: 6,
        paddingHorizontal: 20,
        borderBlockColor: 'grey',
    },

    filterText: {
        fontFamily: 'Regular',
    },

    filterTitle: {
        marginTop: 22,
        alignSelf: 'center',
        fontFamily: 'SemiBold',
    },

    lockerImage: {
        height: 240,
        width: '100%',
        resizeMode: 'contain',
    },

    doubleFilter: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    pickerView: {
        width: 390,
        marginTop: 30,
        borderWidth: 2,
        borderRadius: 280,
        overflow: 'hidden',
    },

    picker: {
        borderWidth: 0,
        marginVertical: 6,
        marginHorizontal: 12,
        backgroundColor: 'transparent',
    },

});

export default AvailLockers;
