
from Core.models import *
from .serializers import *
from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.response import Response

from django.shortcuts import Http404, render
from django.contrib.auth.hashers import check_password, make_password

import random
from .models import *
from django.utils import timezone
from django.http import JsonResponse
from django.utils.timezone import now
from django.core.mail import send_mail

from django.core.exceptions import ObjectDoesNotExist

# ---------------------------------------------------------------------------- #

class CarrerasList(generics.ListAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasCreate(generics.CreateAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasDetail(generics.RetrieveAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasUpdate(generics.UpdateAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

class CarrerasDelete(generics.DestroyAPIView):
    queryset = Carreras.objects.all()
    serializer_class = CarrerasSerializer

# ---------------------------------------------------------------------------- #

class GruposList(generics.ListAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposCreate(generics.CreateAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposDetail(generics.RetrieveAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposUpdate(generics.UpdateAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

class GruposDelete(generics.DestroyAPIView):
    queryset = Grupos.objects.all()
    serializer_class = GruposSerializer

# ---------------------------------------------------------------------------- #

class UsuariosList(generics.ListAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosCreate(generics.CreateAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosDetail(generics.RetrieveAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosUpdate(generics.UpdateAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

class UsuariosDelete(generics.DestroyAPIView):
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer

# ---------------------------------------------------------------------------- #

class CasillerosList(APIView):
    def get(self, request, *args, **kwargs):
        queryset = Casilleros.objects.filter(Disponibilidad=True)
        data = CasillerosSerializer(queryset, many=True).data
        return Response(data)

class CasillerosCreate(generics.CreateAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosDetail(generics.RetrieveAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosUpdate(generics.UpdateAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

class CasillerosDelete(generics.DestroyAPIView):
    queryset = Casilleros.objects.all()
    serializer_class = CasillerosSerializer

# ---------------------------------------------------------------------------- #

class RentasList(generics.ListAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasCreate(generics.CreateAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasDetail(generics.RetrieveAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasUpdate(generics.UpdateAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

class RentasDelete(generics.DestroyAPIView):
    queryset = Rentas.objects.all()
    serializer_class = RentasSerializer

# ---------------------------------------------------------------------------- #

class StickerList(generics.ListAPIView):
    queryset = UsuarioCasillero.objects.all()
    serializer_class = UsuarioCasilleroSerializer

class StickerCreate(generics.CreateAPIView):
    queryset = UsuarioCasillero.objects.all()
    serializer_class = UsuarioCasilleroSerializer

class StickerDetail(generics.RetrieveAPIView):
    queryset = UsuarioCasillero.objects.all()
    serializer_class = UsuarioCasilleroSerializer

class StickerUpdate(generics.UpdateAPIView):
    queryset = UsuarioCasillero.objects.all()
    serializer_class = UsuarioCasilleroSerializer

class StickerDelete(generics.DestroyAPIView):
    queryset = UsuarioCasillero.objects.all()
    serializer_class = UsuarioCasilleroSerializer

# ---------------------------------------------------------------------------- #

class AuthLogin(APIView):

    def post(self, request, *args, **kwargs):
        matricula = request.data.get('Matricula')
        clave = request.data.get('Clave')
        
        try:
            usuario = Usuarios.objects.get(Matricula=matricula)
        except Usuarios.DoesNotExist:
            return Response({"error": "Matrícula no encontrada."}, status=status.HTTP_404_NOT_FOUND)

        if not check_password(clave, usuario.Clave):
            return Response({"error": "Contraseña incorrecta."}, status=status.HTTP_400_BAD_REQUEST)

        return Response(UsuariosSerializer(usuario).data)

# ---------------------------------------------------------------------------- #

class UsuarioMatricula(APIView):

    def get(self, request, matricula, format=None):
        try:
            usuario = Usuarios.objects.get(Matricula=matricula)
            serializer = UsuarioDetallesSerializer(usuario)
            return Response(serializer.data)
        except Usuarios.DoesNotExist:
            return Response({"error": "Usuario no encontrado."}, status=status.HTTP_404_NOT_FOUND)

# ---------------------------------------------------------------------------- #

class SolicitarVerificacion(APIView):

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')

        if not email:
            return JsonResponse({'error': 'Se requiere una dirección de Email.'}, status=400)

        codigo = '{:04d}'.format(random.randint(0, 9999))
        CodigoVerificacion.objects.create(Email=email, Codigo=codigo)

        send_mail(
            'LOCK-IT: Reestablecer contraseña.',
            f'Tu código de verificación es: {codigo}',
            'codewave.utt@gmail.com',

            [email], fail_silently=False,
        )

        return JsonResponse({'message': f'Código enviado a "{email}"'})


class ConfirmarVerificacion(APIView):

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        codigo = request.data.get('codigo')

        if not all([email, codigo]):
            return JsonResponse({'error': 'Se requieren una dirección de Email & el código de verificación.'}, status=400)

        try:
            codigoVerificacion = CodigoVerificacion.objects.get(
                Email=email, Codigo=codigo, FechaCreacion__gte=timezone.now()-timedelta(minutes=4)
            )

            return JsonResponse({'message': 'Código verificado exitosamente.'})

        except CodigoVerificacion.DoesNotExist:
            return JsonResponse({'error': 'Código inválido o expirado.'}, status=400)


class CambiarPassword(APIView):

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        nuevaClave = request.data.get('nuevaPassword')

        if not all([email, nuevaClave]):
            return Response({'error': 'Se requieren una dirección de Email & una nueva contraseña.'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            usuario = Usuarios.objects.get(Email=email)
            usuario.Clave = make_password(nuevaClave)
            usuario.save()

            return Response({'message': 'Contraseña actualizada con éxito.'}, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'error': 'Usuario no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

# ---------------------------------------------------------------------------- #

from datetime import datetime

class RentaCasillero(APIView):

    def post(self, request, *args, **kwargs):
        casillero_id = request.data.get('casilleroID')
        usuario_id = request.data.get('usuarioID')
        # stickers = require.data.get('stickersData')

        if not casillero_id:
            return JsonResponse({"error": "No se proporcionó el ID del casillero."}, status=status.HTTP_400_BAD_REQUEST)
        if not usuario_id:
            return JsonResponse({"error": "No se proporcionó el ID del usuario."}, status=status.HTTP_400_BAD_REQUEST)
        
        # Attempt to retrieve the Casillero and Usuario
        try:
            casillero = Casilleros.objects.get(pk=casillero_id)
        except Casilleros.DoesNotExist:
            return JsonResponse({"error": "Casillero no encontrado."}, status=status.HTTP_404_NOT_FOUND)
        
        try:
            usuario = Usuarios.objects.get(pk=usuario_id)
        except Usuarios.DoesNotExist:
            return JsonResponse({"error": "Usuario no encontrado."}, status=status.HTTP_404_NOT_FOUND)

        if not casillero.Disponibilidad:
            return JsonResponse({"error": "Este casillero no está disponible."}, status=status.HTTP_400_BAD_REQUEST)

        renta = Rentas(
            Usuario=usuario,
            Casillero=casillero,
            RentaInicio=datetime.now(),
            RentaFinal=datetime.now() + timedelta(days=120)
        )
        renta.save()

        stickersUser = UsuarioCasillero(
            Usuario=usuario, Casillero=casillero, Stickers = [],
        )
        stickersUser.save()

        casillero.Disponibilidad = False
        casillero.save()

        renta_serializer = RentasSerializer(renta)
        return JsonResponse(renta_serializer.data, status=status.HTTP_201_CREATED)
