
from Api import views
from django.urls import path

# ---------------------------------------------------------------------------- #

app_name = 'Api'

urlpatterns = [

    # API: Carreras
    path('v1/carreras/list/', views.CarrerasList.as_view(), name='carreras_list'),
    path('v1/carreras/create/', views.CarrerasCreate.as_view(), name='carreras_create'),
    path('v1/carreras/<int:pk>/', views.CarrerasDetail.as_view(), name='carreras_detail'),
    path('v1/carreras/<int:pk>/update/', views.CarrerasUpdate.as_view(), name='carreras_update'),
    path('v1/carreras/<int:pk>/delete/', views.CarrerasDelete.as_view(), name='carreras_delete'),

    # API: Grupos
    path('v1/grupos/list/', views.GruposList.as_view(), name='grupos_list'),
    path('v1/grupos/create/', views.GruposCreate.as_view(), name='grupos_create'),
    path('v1/grupos/<int:pk>/', views.GruposDetail.as_view(), name='grupos_detail'),
    path('v1/grupos/<int:pk>/update/', views.GruposUpdate.as_view(), name='grupos_update'),
    path('v1/grupos/<int:pk>/delete/', views.GruposDelete.as_view(), name='grupos_delete'),

    # API: Usuarios
    path('v1/usuarios/list/', views.UsuariosList.as_view(), name='usuarios_list'),
    path('v1/usuarios/create/', views.UsuariosCreate.as_view(), name='usuarios_create'),
    path('v1/usuarios/<int:pk>/', views.UsuariosDetail.as_view(), name='usuarios_detail'),
    path('v1/usuarios/<int:pk>/update/', views.UsuariosUpdate.as_view(), name='usuarios_update'),
    path('v1/usuarios/<int:pk>/delete/', views.UsuariosDelete.as_view(), name='usuarios_delete'),

    # API: Casilleros
    path('v1/casilleros/list/', views.CasillerosList.as_view(), name='casilleros_list'),
    path('v1/casilleros/create/', views.CasillerosCreate.as_view(), name='casilleros_create'),
    path('v1/casilleros/<int:pk>/', views.CasillerosDetail.as_view(), name='casilleros_detail'),
    path('v1/casilleros/<int:pk>/update/', views.CasillerosUpdate.as_view(), name='casilleros_update'),
    path('v1/casilleros/<int:pk>/delete/', views.CasillerosDelete.as_view(), name='casilleros_delete'),

    # API: Rentas
    path('v1/rentas/list/', views.RentasList.as_view(), name='rentas_list'),
    path('v1/rentas/create/', views.RentasCreate.as_view(), name='rentas_create'),
    path('v1/rentas/<int:pk>/', views.RentasDetail.as_view(), name='rentas_detail'),
    path('v1/rentas/<int:pk>/update/', views.RentasUpdate.as_view(), name='rentas_update'),
    path('v1/rentas/<int:pk>/delete/', views.RentasDelete.as_view(), name='rentas_delete'),

    # API: Stickers
    path('v1/stickers/list/', views.StickerList.as_view(), name='stickers_list'),
    path('v1/stickers/create/', views.StickerCreate.as_view(), name='stickers_create'),
    path('v1/stickers/<int:pk>/', views.StickerDetail.as_view(), name='stickers_detail'),
    path('v1/stickers/<int:pk>/update/', views.StickerUpdate.as_view(), name='stickers_update'),
    path('v1/stickers/<int:pk>/delete/', views.StickerDelete.as_view(), name='stickers_delete'),

    # ------------------------------------------------------------------------ #

    path('v1/login/', views.AuthLogin.as_view(), name='authLogin'),
    path('v1/usuario/detalles/<str:matricula>/', views.UsuarioMatricula.as_view(), name='usuario_detalles'),
    path('v1/solicitarCodigo/', views.SolicitarVerificacion.as_view(), name='solicitar_codigo'),
    path('v1/verificarCodigo/', views.ConfirmarVerificacion.as_view(), name='verificar_codigo'),
    path('v1/cambiarPassword/', views.CambiarPassword.as_view(), name='password_update'),

    path('v1/casillero/paypal/', views.RentaCasillero.as_view(), name='Renta_Casillero'),
]

# ---------------------------------------------------------------------------- #
