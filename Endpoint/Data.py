
# python manage.py shell < Data.py

import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Endpoint.settings')
django.setup()

from Core.models import *

# ---------------------------------------------------------------------------- #

## Creación de registros para Carreras.
Carreritas = [
    Carreras(Nombre='Contaduría'),
    Carreras(Nombre='Desarrollo & Gestión de Software'),
    Carreras(Nombre='Electromecánica Industrial'),
    Carreras(Nombre='Energías Renovables'),
    Carreras(Nombre='Entornos Virtuales & Negocios Digitales'),
    Carreras(Nombre='Gestión de Redes Logísticas'),
    Carreras(Nombre='Innovación de Negocios & Mercadotecnia'),
    Carreras(Nombre='Logística Comercial Global'),
    Carreras(Nombre='Manufactura Aeronáutica'),
    Carreras(Nombre='Mecatrónica'),
    Carreras(Nombre='Procesos & Operaciones Industriales'),
    Carreras(Nombre='Redes Inteligentes & Ciberseguridad'),
    Carreras(Nombre='Tecnología Ambiental'),
]

Carreras.objects.bulk_create(Carreritas)

print("Carreras insertadas con éxito en la BD.")


## Creación de registros para Grupos.
Grupillos = [
    Grupos(Grado='1ro', Grupo='A'),
    Grupos(Grado='1ro', Grupo='B'),
    Grupos(Grado='1ro', Grupo='C'),
    Grupos(Grado='1ro', Grupo='D'),
    Grupos(Grado='1ro', Grupo='E'),

    Grupos(Grado='2do', Grupo='A'),
    Grupos(Grado='2do', Grupo='B'),
    Grupos(Grado='2do', Grupo='C'),
    Grupos(Grado='2do', Grupo='D'),
    Grupos(Grado='2do', Grupo='E'),

    Grupos(Grado='3ro', Grupo='A'),
    Grupos(Grado='3ro', Grupo='B'),
    Grupos(Grado='3ro', Grupo='C'),
    Grupos(Grado='3ro', Grupo='D'),
    Grupos(Grado='3ro', Grupo='E'),

    Grupos(Grado='4to', Grupo='A'),
    Grupos(Grado='4to', Grupo='B'),
    Grupos(Grado='4to', Grupo='C'),
    Grupos(Grado='4to', Grupo='D'),
    Grupos(Grado='4to', Grupo='E'),

    Grupos(Grado='5to', Grupo='A'),
    Grupos(Grado='5to', Grupo='B'),
    Grupos(Grado='5to', Grupo='C'),
    Grupos(Grado='5to', Grupo='D'),
    Grupos(Grado='5to', Grupo='E'),

    Grupos(Grado='6to', Grupo='A'),
    Grupos(Grado='6to', Grupo='B'),
    Grupos(Grado='6to', Grupo='C'),
    Grupos(Grado='6to', Grupo='D'),
    Grupos(Grado='6to', Grupo='E'),

    Grupos(Grado='7mo', Grupo='A'),
    Grupos(Grado='7mo', Grupo='B'),
    Grupos(Grado='7mo', Grupo='C'),
    Grupos(Grado='7mo', Grupo='D'),
    Grupos(Grado='7mo', Grupo='E'),

    Grupos(Grado='8vo', Grupo='A'),
    Grupos(Grado='8vo', Grupo='B'),
    Grupos(Grado='8vo', Grupo='C'),
    Grupos(Grado='8vo', Grupo='D'),
    Grupos(Grado='8vo', Grupo='E'),

    Grupos(Grado='9no', Grupo='A'),
    Grupos(Grado='9no', Grupo='B'),
    Grupos(Grado='9no', Grupo='C'),
    Grupos(Grado='9no', Grupo='D'),
    Grupos(Grado='9no', Grupo='E'),
]

Grupos.objects.bulk_create(Grupillos)

print("Grupos insertados con éxito en la BD.")


## Creación de registros para Usuarios, en orden:
UsuariosBase = [

    # Alumnos:
    ('0322103814', 'EdPassword', 'Ed', 'Rubio', None, 'Masculino', '2000-11-14', '(664) 264-4682', 'ed.cetacean@outlook.com', 'Alumno', 'usuarios/perfil/0322103814.jpg', 'usuarios/portada/0322103814.jpg'),
    ('0322103724', 'GaboPassword', 'Gabriel A.', 'Gómez', 'Ramírez', 'Masculino', '2002-02-12', '(664) 753-2947', 'gabo@outlook.com', 'Alumno', None, None),
    ('0322103746', 'JaimePassword', 'Jaime I.', 'López', 'Guerrero', 'Masculino', '2004-06-26', '(664) 599-6028', 'cyberhackz@outlook.com', 'Alumno', None, None),
    ('0322103778', 'StevePassword', 'Steve O.', 'Labastida', None, 'Masculino', '1998-02-08', '(663) 126-1566', 'olmos@outlook.com', 'Alumno', None, None),
    ('0322101923', 'HugoPassword', 'Hugo', 'Macareno', 'Rivera', 'Masculino', '2004-12-10', '(664) 264-4682', 'edith.waifu@outlook.com', 'Alumno', None, None),

    # Docentes:
    ('0322100001', 'RileyPassword', 'Riley', 'Smith', None, 'Masculino', '1994-08-14', '(664) 944-5897', 'riley.smith@outlook.com', 'Docente', None, None),
    ('0322100002', 'EmiPassword', 'Emi', 'García', None, 'Femenino', '1996-04-12', '(664) 935-1688', 'emili@outlook.com', 'Docente', None, None),

    # Administrativos:
    ('0322100003', 'MillerPassword', 'Miller', 'Brown', None, 'Masculino', '1984-08-26', '(664) 126-9588', 'miller@outlook.com', 'Administrativo', None, None),
    ('0322100004', 'KarlPassword', 'Carl', 'Johnson', None, 'Masculino', '1988-07-22', '(664) 193-6697', 'cj.gta@outlook.com', 'Administrativo', None, None),
]

for Matri, Contra, Nomb, FirstA, SecondA, Genre, Nacimiento, Celular, Correo, Ocupat, MainFotillo, SubFotillo in UsuariosBase:

    Usuarios.objects.create(
        Matricula=Matri, Clave=Contra, Nombres=Nomb, ApellidoPaterno=FirstA, ApellidoMaterno=SecondA,
        Genero=Genre, Natalicio=Nacimiento, NumTel=Celular, Email=Correo, Ocupacion=Ocupat, FotoPerfil=MainFotillo, FotoPortada=SubFotillo,
    )

print("Usuarios insertados con éxito en la BD.")


def insertarCasilleros():

    for i in range(55, 100):
        Dispo = True if i % 2 == 0 else False
        Tam = "Normal" if i % 2 == 0 else "Grande"
        Price = "80" if Tam == "Normal" else "120"
        Doce = f"Docencia {i % 5 + 1}"
        PisoBA = "Planta Baja" if i % 2 == 0 else "Planta Alta"

        nuevoCasillero = Casilleros(
            Disponibilidad=Dispo,
            Tamano=Tam,
            Precio=Price,
            Docencia=Doce,
            Piso=PisoBA
        )

        nuevoCasillero.save()
        print(f"Casillero {i} agregado con éxito.")

insertarCasilleros()

# ---------------------------------------------------------------------------- #

print("Datos insertados con éxito en la BD.")